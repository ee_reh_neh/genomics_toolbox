#!/bin/bash
set -eu
# John Blischak
# 2013-05-09
# Maps reads with BWA and performs initial preprocessing.
# Use the -h commands for usage instructions.
################################################################################

# Default options
outdir=""
name=""
paired=false
n=2
q=10
cancel=false
rmdup=false
substitution=""
bam=false
# Paths to programs	
BWA_PATH="/mnt/lustre/home/jdblischak/programs/bwa-0.5.9rc1"				
BEDTOOLS_PATH="/mnt/lustre/home/jdblischak/programs/BEDTools-Version-2.12.0/bin"	

usage() {
  echo "Usage: `basename $0` [options] <fastq file> <genome fasta file>"
  echo "Options:"
  echo "  -h                   print this help"
  echo "  -o <dir>             write new files to this output directory"
  echo "  -f <name>            use this new name for files"
  echo "  -p                   reads are paired end"
  echo "  -n                   number of mismatches allowed [default 2]"
  echo "  -q                   quality cutoff for mapped reads [default 10]"
  echo "  -c                   cancel script if files already exist"
  echo "  -r                   remove duplicate reads"
  echo "  -s <sub>             modify filename with this sed subsitution (cannot be used with -f)"
  echo "  -b                   input file is in bam format"
}

while getopts ":ho:f:pn:q:crs:b" opt
do
case $opt in
  h)
    usage
    exit 0
    ;;
  o)
    outdir=${OPTARG%/}    
    echo "Files will be written to $outdir." >&2
    ;;
  f)
    name=`basename $OPTARG`
    echo "Files will be named $name." >&2
    ;;
  p)
    paired=true
    echo "Mapping as paired end reads." >&2
    ;;
  n)
    n=$OPTARG
    ;;
  q)
    q=$OPTARG
    ;;
  c)
    cancel=true
    echo "Analysis will not run if output files already exist." >&2
    ;;
  r)
    rmdup=true
    echo "Removing duplicate reads." >&2
    ;;
  s)
    substitution=$OPTARG
    ;;
  b)
    bam=true
    ;;
  \?)
    echo "Invalid option: -$OPTARG" >&2
    exit 1
    ;;
  :)
    echo "Option -$OPTARG requires an argument." >&2
    exit 1
    ;;
esac
done  

# Shift the command line index to start at 1 for the first positional argument
shift $((OPTIND - 1))

if [ "$#" -ne 2 ]
then
  usage
  exit 2
else
  fastq=$1
  genome=$2
fi

# Assign default out directory and filename if not user-supplied
if [ -z "$name" ]
then
  name=`basename $fastq`
  name=${name%".gz"}
  name=${name%".fastq"}
  name=${name%".txt"}
  if [ -n "$substitution" ]
  then
      name=`echo $name | sed $substitution`
  fi
  echo "The base filename will be $name." >&2
fi

# Determine the output directory
if [ -z "$outdir" ]
then
  outdir=${fastq%`basename $fastq`}
  if [ -z "$outdir" ]
  then
    outdir="."
  else outdir=${outdir%/}
  fi
  echo "Files will be written to the following directory: $outdir" >&2
fi

# Cancel the script if the -c option was supplied and the analysis
# has previously been run.
if $cancel && [ -e $outdir/$name.quality.sorted.nodup.bam.gz ]
then
  echo "Script stopped because $outdir/$name.quality.sorted.nodup.bam.gz already exists." >&2
  exit 1
fi

echo "Running bwa aln allowing $n mismatches..." >&2
if $bam
then
  echo "Input file is in bam format." >&2
  $BWA_PATH/bwa aln -n $n $genome -b $fastq > $outdir/$name.sai
else
  $BWA_PATH/bwa aln -n $n $genome $fastq > $outdir/$name.sai
fi

if $paired
then
  echo "Running bwa sampe..." >&2
  $BWA_PATH/bwa sampe -n $n $genome  $outdir/$name.sai $fastq > $outdir/$name.sam
else
  echo "Running bwa samse..." >&2
  $BWA_PATH/bwa samse -n 1 $genome  $outdir/$name.sai $fastq > $outdir/$name.sam
fi

echo "Separating mapped (quality > $q) and unmapped reads..." >&2
samtools view -S -h -F 4 -b $outdir/$name.sam > $outdir/$name.mapped.bam
samtools view -S -h -q 10 -F 4 -b $outdir/$name.sam > $outdir/$name.quality.bam
samtools view -S -h -f 4 -b $outdir/$name.sam > $outdir/$name.unmapped.bam
echo "Sorting reads..." >&2
samtools sort $outdir/$name.quality.bam $outdir/$name.quality.sorted
if $rmdup
then
  echo "Removing duplicates..." >&2
  samtools rmdup -s $outdir/$name.quality.sorted.bam $outdir/$name.quality.sorted.nodup.bam
  echo "Creating bed file..." >&2
  $BEDTOOLS_PATH/bamToBed -i $outdir/$name.quality.sorted.nodup.bam > $outdir/$name.quality.sorted.nodup.bed
  echo "Counting reads..." >&2
  echo -ne "raw\t" > $outdir/$name.counts.txt
  samtools view -S -c $outdir/$name.sam >> $outdir/$name.counts.txt
  echo -ne "mapped\t" >> $outdir/$name.counts.txt
  samtools view -c $outdir/$name.mapped.bam >> $outdir/$name.counts.txt
  echo -ne "quality_mapped\t" >> $outdir/$name.counts.txt
  samtools view -c $outdir/$name.quality.sorted.bam >> $outdir/$name.counts.txt
  echo -ne "no_dups\t" >> $outdir/$name.counts.txt 
  samtools view -c $outdir/$name.quality.sorted.nodup.bam >> $outdir/$name.counts.txt 
  echo -ne "unmapped\t" >> $outdir/$name.counts.txt
  samtools view -c $outdir/$name.unmapped.bam >> $outdir/$name.counts.txt
else
  echo "Creating bed file..." >&2
  $BEDTOOLS_PATH/bamToBed -i $outdir/$name.quality.sorted.bam > $outdir/$name.quality.sorted.bed
  echo "Counting reads..." >&2
  echo -ne "raw\t" > $outdir/$name.counts.txt
  samtools view -S -c $outdir/$name.sam >> $outdir/$name.counts.txt
  echo -ne "mapped\t" >> $outdir/$name.counts.txt
  samtools view -c $outdir/$name.mapped.bam >> $outdir/$name.counts.txt
  echo -ne "quality_mapped\t" >> $outdir/$name.counts.txt
  samtools view -c $outdir/$name.quality.sorted.bam >> $outdir/$name.counts.txt
  echo -ne "unmapped\t" >> $outdir/$name.counts.txt
  samtools view -c $outdir/$name.unmapped.bam >> $outdir/$name.counts.txt
fi

echo "Removing sai and sam files. gzipping bam files..." >&2
rm $outdir/$name.sa[i,m]
gzip -f $outdir/$name.*.bam
if [ -e $outdir/core.* ]
then
  echo "WARNING: BWA core dumped. Must be re-run!" >&2
  exit 1
fi
echo "Finished successfully." >&2
exit 0






