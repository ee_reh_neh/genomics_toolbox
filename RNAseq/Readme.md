# RNA sequencing

###Create bed file of exons for RNAseq
    :::bash
       # Using human as an example
       # Only including protein-coding genes on the autosomes
       ensembl_exons.R -a -b protein_coding > exons_human.bed
       # Merge partially or fully overlapping exons for a 
       # given gene model. Also, removes any merged exon units
       # that are the result of overlaps of more than one gene.
       clean_exons.py exons_human.bed > exons_human_clean.bed

###Contact:
John Blischak
jdblischak at gmail dot com