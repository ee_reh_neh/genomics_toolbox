#!/bin/bash
set -eu
## John Blischak
## 2013 04 26
## Perform FastQC on the gzipped fastq file provided on the command line.

if [ "$#" -ne 1 ]
then 
  echo "Must supply the name of the gzipped fastq file at the command line." >&2
  echo "bash run_fastq.sh <filename.fastq.gz>" >&2
  exit 1
fi

fastq=$1
echo $fastq >&2
base=`basename $fastq`
dir=${fastq%$base}

mkdir -p ${dir}fastqc
# Using zcat because gzip does not work if the file is a symbolic link
echo "Creating temporary fastq file..." >&2
zcat $fastq > /tmp/${base%".gz"}
echo "Running fastqc..." >&2
/mnt/lustre/data/tools/FastQC/fastqc --outdir ${dir}fastqc /tmp/${base%".gz"}
echo "Rmoving temporary fastq file..." >&2
rm /tmp/${base%".gz"}
echo "Finished successfully." >&2
exit 0
